wallThick = 2;  //sets width of wall at base
cutterMinimum = .8;  //sets width of wall at top
baseHeight = 2;
height = 20;
flangeWidth = 5;

minkowski(){
outline();
cylinder(r1 = wallThick/3, r2 = cutterMinimum/3, h = height);
};
//%cube([.5, .5, height*3], true);

flange();

module flange(){
	difference(){
	minkowski(){
	baseShape(baseHeight/3);
	cylinder(r = flangeWidth, h = baseHeight/3);
	}
	translate([0,0,-0.01])baseShape(baseHeight*1.1);
	};
}



module outline(){
	difference(){
	minkowski(){
	baseShape(baseHeight/3);
	cylinder(r = cutterMinimum/3, h = baseHeight/3);
	}
	translate([0,0,-0.01])baseShape(baseHeight*1.1);
	};
};

module baseShape(H){
linear_extrude(file = "turkey.dxf", height=H);
}